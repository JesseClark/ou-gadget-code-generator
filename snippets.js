/*
var snippets = {
 'group1': {
     'list': [
         { id, text, description, help_url, snippet_url },
         ...
         { id, text, description, help_url, snippet_url }
     ]
 },
 ...
 'group99': {
     'list': [
         { id, text, description, help_url, snippet_url },
         ...
         { id, text, description, help_url, snippet_url }
     ]
 }   
}
*/

var snippets = {
    'Everyone': {
        'list': [
            {
                id: 'sample1',
                text: 'Sample Snippet #1',
                description: 'This is an example snippet.',
                help_url: 'http://wikis.ttu.edu/',
                snippet_url: 'snippets/sample1.txt'
            },
            {
                id: 'sample2',
                text: 'Sample Snippet #2',
                description: 'This is another example snippet.',
                help_url: 'http://wikis.ttu.edu/',
                snippet_url: 'snippets/sample1.txt'
            }
        ] // end of 'Everyone' list
    },
    '_Advanced': {
        'list': [
            {
                id: 'advanced1',
                text: 'Advanced Snippet #1',
                description: 'This is an example snippet.',
                help_url: 'http://wikis.ttu.edu/',
                snippet_url: 'snippets/sample1.txt'
            },
            {
                id: 'advanced2',
                text: 'Sample Snippet #2',
                description: 'This is another example snippet.',
                help_url: 'http://wikis.ttu.edu/',
                snippet_url: 'snippets/sample1.txt'
            }
        ] // end of 'Everyone' list
    }
  
}




   
